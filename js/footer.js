let footer = document.createElement("footer");

footer.innerHTML = 
`<ul class="footer-links">
<li id="mail"><a href="mailto:contact@QuickFood.com">contact@QuickFood.com</a></li>
<li id="tlf"><a href="tel:55545251">55 54 53 52 51</a></li>
<li id="map"><a href="https://goo.gl/maps/yVjypn6LNuaf8Ecz6">Høgskoleringen 1, 7491 Trondheim</a></li>
<li id="copyright">© 2021 QuickFood</li>
</ul>`

body.append(footer)