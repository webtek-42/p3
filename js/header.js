let body = document.body;

let header = document.createElement("header");

header.innerHTML = 
`<nav>
<div class="burger">
    <div class="line1"></div>
    <div class="line2"></div>
    <div class="line3"></div>
</div>
<div class="logo">
    <a href="index.html">
        <img src="img/QuickFood logo.png" alt="logo">
    </a>
</div>
<ul class="nav-links">
    <li>
        <a href="meny.html">Meny</a>
    </li>
    <li>
        <a href="min_side.html">Min side</a>
    </li>
    <li>
        <a href="om_oss.html">Om oss</a>
    </li>
</ul>

<div class="cart" id="navCart" onclick="toggleHandlekurvVindu()">
    <img src="img/cart.svg" alt="cart">
</div>
</nav>`

body.prepend(header);
