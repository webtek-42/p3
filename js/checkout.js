//Henter lista hvor ordren skal skrives ut
let ul = document.getElementById("coVarer");

oppdaterCheckout();
// oppdaterCheckout skriver ut ordene i handlekurven i lista hentet ovenfor
function oppdaterCheckout(){
    ul.innerHTML = "";
    let subtotal = 0;
    //Sjekker om handlekurven er tom
    if(handlekurv.length == 0){
        let li = document.createElement("li");
        ul.appendChild(li);
        li.innerText = "Handlekurven er tom";
    
    //Dersom handlekurven ikke er tom så itererer over handlekurven og lager nytt punkt for hver rett
    }else{
        for(var vare of handlekurv){
            //Regner ut pris og subtotal
            let antall = vare.antall;
            let pris = vare.pris * antall;
            subtotal += pris;
            let navn = vare.navn;
            let pointer = vare;

            let li = document.createElement("li");
            ul.appendChild(li);

            let venstre = document.createElement("div");
            li.appendChild(venstre);
            venstre.className = "coVareVen";
            venstre.innerText += " " + antall + " stk " + navn;

            let minus = document.createElement("button");
            minus.className = "coVareKnapp";
            minus.innerText = "-";
            minus.addEventListener("click", function(){trekkFra(pointer);});

            let pluss = document.createElement("button");
            pluss.className = "coVareKnapp";
            pluss.innerText = "+";
            pluss.addEventListener("click", function(){leggTil(pointer);});


            venstre.prepend(pluss);
            venstre.prepend(minus);

            let hoyre = document.createElement("div");
            li.appendChild(hoyre);
            hoyre.className = "coVareHoy";
            hoyre.innerText = pris + ",-";
        }
    }
    //Kaller pris med subtotal som er regnet ut, og settTid slik at leveringstid er oppdatert
    pris(subtotal);
    settTid();  
}

//pris tar inn et tall, subtotal og regner ut hva levering og total pris er, samt skriver dem ut
function pris(subtotalPris){
    let subtotal = document.getElementById("subtotal");
    let levering = document.getElementById("levering");
    let total = document.getElementById("total");

    subtotal.innerHTML = subtotalPris + ",-";
    levering.innerHTML = "40,-";
    total.innerHTML = (subtotalPris + 40) + ",-";
}

//settTid henter tiden og skriver ut tider for levering
function settTid(){
    const date = new Date();
    let time = date.getHours();
    let min = date.getMinutes();

    let select = document.getElementById("coTimeSelect");
    select.innerHTML = "";

    let delmin = 0;
    let deltime = 0;
    let tidut;

    //Lager en option for hvert 5 min etter mellm 20-40 min etter nåværende tid
    for(let i = 20; i<45; i = i+5){
        tidut = "";
        deltime = time;
        delmin = min + i;

        if((delmin/60) > 1){
            deltime++;
            if(deltime > 23){
                deltime = 0;
            }
        }
        
        delmin = delmin%60;
        if(delmin < 10){
            delmin = "0" + delmin;
        }

        let option = document.createElement("option");
        option.innerText = deltime + ":" + delmin;
        select.appendChild(option);

    }
}

//trekkFra tar inn et vare objekt og trekker en av disse fra handlekurven
function trekkFra(vare){
    for(var item of handlekurv){
        if(item.index == vare.index){
            if(item.antall > 1){
                item.antall--;
            }else{
                handlekurv.splice(handlekurv.indexOf(item), 1);
            }
        }
    }

    localStorage.setItem("handlekurv", JSON.stringify(handlekurv));
    oppdaterCheckout();
}

//leggTil tar inn et vare objekt og legger til en av disse i handlekurven, så sant den er i kurven fra før av 
function leggTil(vare){
    for(var item of handlekurv){
        if(item.index == vare.index){
            item.antall++;
        }
    }

    localStorage.setItem("handlekurv", JSON.stringify(handlekurv));
    oppdaterCheckout();
}

//bestill later som en bestilling går igjennom
function bestill(){
    //Sjekker om handlekurven er tom, sender en melding dersom det er slik
    if(handlekurv.length == 0){
        ul = document.getElementById("coVarer");
        ul.innerHTML = "";

        let li = document.createElement("li");
        ul.appendChild(li);
        li.innerText = "Vennligst legg til noe i handlekurven!";

    //Ellers clearer den handlekurven og skriver ut takk for bestilling
    }else{
        localStorage.clear();
        localStorage.setItem("handlekurv", JSON.stringify([]));

        ul = document.getElementById("coVarer");
        ul.innerHTML = "";

        let li = document.createElement("li");
        ul.appendChild(li);
        li.innerText = "Takk for bestillingen!";

    }
}