// Henter elementene som skal gjøre noe med
const OBknapp = document.getElementById("OBknapp");
const loggInn = document.getElementById("loggInn");
const brukernavn = document.getElementById("brukernavn");
const passord = document.getElementById("passord");

// Funkjson som gir brukeren beskjed om brukernavn eller passord mangler eller begge. Brukes for "opprett bruker"
function opprettetBruker() {

    if (brukernavn.value == "" && passord.value == "") {
        alert("brukernavn og passord mangler");
    }

    else if (brukernavn.value == "") {
        alert("Vennlist skriv inn et brukernavn");
    }

    else if (passord.value == "") {
        alert("Vennligst skriv inn et passord");
    }

    else {
        alert("Bruker opprettet");
    }
}

// Bruker eventLister for å koble funksjonen opp mot "Opprett bruker"
OBknapp.addEventListener("click", opprettetBruker);

// Funkjson som gir brukeren beskjed om brukernavn eller passord mangler eller begge. Brukes for "Logg inn"
function loginBruker() {

    if (brukernavn.value == "" && passord.value == "") {
        alert("brukernavn og passord mangler");
    }

    else if (brukernavn.value == "") {
        alert("Vennlist skriv inn et brukernavn");
    }

    else if (passord.value == "") {
        alert("Vennligst skriv inn et passord");
    }

    else {
        alert("Bruker logget inn");
    }
}

// Bruker eventLister for å koble funksjonen opp mot "Logg inn"
loggInn.addEventListener("click", loginBruker);