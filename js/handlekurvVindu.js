//Sjekker om det er en handlekurv i localStorage, dersom det ikke er det lager den en handlekurv.
//Enn så lenge er det logger av handlekurven så en kan se hva som skjer
if(localStorage.getItem("handlekurv") == null){
    const handlekurv = [];
    localStorage.setItem("handlekurv", JSON.stringify(handlekurv));
}

const handlekurv = JSON.parse(localStorage.getItem("handlekurv"));
var toggle = false;

//toggleHandlekurvVindu sjekker om vinduet er på eller av og henholdvis fjerner eller legger til vinduet
function toggleHandlekurvVindu(){
    if(toggle){
        let main = document.getElementById("hkMain");
        document.getElementById("navCart").removeChild(main);
        toggle = false;
    }else{
        lagVindu();
        toggle = true;
    }
}

//lagVindu bygger opp strukturen til handlekurv vinduet og legger det til body
function lagVindu(){
    let cart = document.getElementById("navCart");

    let hkMain = document.createElement("div");
    hkMain.id = "hkMain";
    cart.appendChild(hkMain);

    let hkBody = document.createElement("div");
    hkBody.id = "hkBody";
    hkMain.appendChild(hkBody);

    hkBody.innerText = "Handlekurv:";

    let hkListe = document.createElement("ul");
    hkBody.appendChild(hkListe);
    hkListe.id = "hkListe";


    if(handlekurv.length == 0){
        let li = document.createElement("li");
        hkListe.appendChild(li);
        li.innerText = "Handlekurven er tom";
    }else{
        for(var vare of handlekurv){
            let antall = vare.antall;
            let pris = vare.pris * antall;
            let navn = vare.navn;

            let li = document.createElement("li");
            hkListe.appendChild(li);
            let venstre = document.createElement("div");
            li.appendChild(venstre);
            venstre.className = "hkListeV";
            venstre.innerText = antall + " stk " + navn;

            let hoyre = document.createElement("div");
            li.appendChild(hoyre);
            hoyre.className = "hkListeH";
            hoyre.innerText = pris + ",-";

        }
    }

    let link = document.createElement("a");
    link.href = "handlekurv.html";

    let button = document.createElement("button");
    button.id = "hkKnapp";
    button.innerText = "Gå til checkout";

    link.appendChild(button);
    hkBody.appendChild(link);
}