if(localStorage.getItem("handlekurv") == null){
    const handlekurv = [];
    localStorage.setItem("handlekurv", JSON.stringify(handlekurv));
}

const varer = [
    {
        index : 1,
        navn : "Hamburger",
        pris : 120,
        antall : 0,
        type: "Burger",
        beskrivelse: "Storfekjøtt, hamburgerbrød, løk, salat, tomat, dressing"
    },
    {
        index : 2,
        navn : "Cheeseburger",
        pris : 130,
        antall : 0,
        type: "Burger",
        beskrivelse: "Storfekjøtt, hamburgerbrød, løk, salat, tomat, cheddar"
    },
    {
        index : 3,
        navn : "BBQ-burger",
        pris : 160,
        antall : 0,
        type: "Burger",
        beskrivelse: "Storfekjøtt, hamburgerbrød, løk, salat, tomat, cheddar, BBQ saus"
    },
    {
        index : 4,
        navn : "Chilli-cheeseburger",
        pris : 160,
        antall : 0,
        type: "Burger",
        beskrivelse: "Storfekjøtt, hamburgerbrød, løk, salat, tomat, cheddar, srirachasaus,"
    },
    {
        index : 5,
        navn : "Blå-burger",
        pris : 160,
        antall : 0,
        type: "Burger",
        beskrivelse: "Storfekjøtt, hamburgerbrød, salat, tomat, blåmuggost, karamelisert løk"
    },
    {
        index : 6,
        navn : "Vegetar Burger",
        pris : 150,
        antall : 0,
        type: "Burger",
        beskrivelse: "Incredible-burger patty, hamburgerbrød, salat, tomat, dressing, cheddar"
    },
    {
        index : 7,
        navn : "Margherita",
        pris : 110,
        antall : 0,
        type: "Pizza",
        beskrivelse: "Pizzabunn, tomatsaus, ost, basilikum"
    },
    {
        index : 8,
        navn : "Ost og skinke",
        pris : 130,
        antall : 0,
        type: "Pizza",
        beskrivelse: "Pizzabunn, tomatsaus, ost, skinke, basilikum"
    },
    {
        index : 9,
        navn : "Peperoni",
        pris : 130,
        antall : 0,
        type: "Pizza",
        beskrivelse: "Pizzabunn, tomatsaus, ost, basilikum"
    },
    {
        index : 10,
        navn : "Hawaii",
        pris : 130,
        antall : 0,
        type: "Pizza",
        beskrivelse: "Pizzabunn, tomatsaus, ost, skinke, annanas"
    },
    {
        index : 11,
        navn : "Chicago",
        pris : 140,
        antall : 0,
        type: "Pizza",
        beskrivelse: "Deep dish pizzabunn, tomatsaus, ost, biffstrimler, bbq-saus"
    },
    {
        index : 12,
        navn : "Prosciutto",
        pris : 130,
        antall : 0,
        type: "Pizza",
        beskrivelse: "Pizzabunn, tomatsaus, parmesan, serrano skinke, ruccola, oliven"
    },
    {
        index : 13,
        navn : "Chilli Dragon",
        pris : 140,
        antall : 0,
        type: "Pizza",
        beskrivelse: "Pizzabunn, spicy tomatsaus, kylling, tomat, ruccola"
    },
    {
        index : 14,
        navn : "Chicken Bombastick",
        pris : 160,
        antall : 0,
        type: "Pizza",
        beskrivelse: "Pizzabunn, tomatsaus, ost, kyllingstrimler, bacon, bbq-saus"
    },
    {
        index :15,
        navn : "Flamkoken",
        pris : 130,
        antall : 0,
        type: "Pizza",
        beskrivelse: "Pizzabunn, hvitsaus, bacon, løk, ost, paprikakrydder"
    },
    {
        index :16,
        navn : "Vegetar",
        pris : 130,
        antall : 0,
        type: "Pizza",
        beskrivelse: "Pizzabunn, hvitsaus, sopp, squash, aubergine, fetaost"
    },
    {
        index : 17,
        navn : "Coca Cola 0,5l",
        pris : 48,
        antall : 0,
        type: "Drikke",
        beskrivelse: ""
    },
    {
        index : 18,
        navn : "Coca Cola 1,5l",
        pris : 80,
        antall : 0,
        type: "Drikke",
        beskrivelse: ""
    },
    {
        index : 19,
        navn : "Coca Cola Zero 0,5l",
        pris : 48,
        antall : 0,
        type: "Drikke",
        beskrivelse: ""
    },
    {
        index : 20,
        navn : "Fanta 0,5l",
        pris : 48,
        antall : 0,
        type: "Drikke",
        beskrivelse: ""
    },
    {
        index : 21,
        navn : "Fanta 1,5l",
        pris : 80,
        antall : 0,
        type: "Drikke",
        beskrivelse: ""
    },
    {
        index : 22,
        navn : "Sprite 0,5l",
        pris : 48,
        antall : 0,
        type: "Drikke",
        beskrivelse: ""
    },
    {
        index : 23,
        navn : "Imsdal Vann 0,5l",
        pris : 38,
        antall : 0,
        type: "Drikke",
        beskrivelse: ""
    },
    {
        index : 24,
        navn : "Imsdal Farris 0,5l",
        pris : 38,
        antall : 0,
        type: "Drikke",
        beskrivelse: ""
    },
];

const typer = ["Burger", "Pizza", "Drikke"];


for(var typ of typer){
    for(var rett of varer){
        if(rett.type == typ){
            leggTilListeElem(rett, typ);
        }
    }
}

function leggTilListeElem(rett, type){
    let rettNavn = rett.navn;
    let rettPris = rett.pris;
    let rettBesk = rett.beskrivelse;

    let liste = document.getElementById("menyListe" + type);

    let li = document.createElement("li");

    let navn = document.createElement("div");
    navn.className = "menyListeVenstre";
    navn.innerText = rettNavn;

    let pris = document.createElement("div");
    pris.className = "menyListeHoyre";
    pris.innerText = rettPris + ",-";

    let linje = document.createElement("div");
    linje.className = "menyLinje";

    let besk = document.createElement("div");
    besk.className = "menyListeVenstre menyListeBesk";
    besk.innerText = rettBesk;

    let butt = document.createElement("div");
    butt.className = "menyListeHoyre";

    let button = document.createElement("button");
    button.className = "menybutton";
    button.addEventListener("click", function(){leggTilHandlekurv(rettNavn);});
    button.innerText = "Legg til handlekurv";
    butt.appendChild(button);

    li.appendChild(navn);
    li.appendChild(pris);
    li.appendChild(linje);
    li.appendChild(besk);
    li.appendChild(butt);

    liste.appendChild(li);
}
    
    function leggTilHandlekurv(vare){
        let navn = null;
    
        for(var item of handlekurv){
            if(item.navn == vare){
                navn = item;
                navn.antall++;
            }
        }

        if(!navn){
            for(item of varer){
                if(item.navn == vare){
                    navn = item;
                }
            }

            handlekurv.push(navn);
            navn.antall++;
        }


    
        sorterListe();

        localStorage.setItem("handlekurv", JSON.stringify(handlekurv));
    }
    
    function sorterListe(){
        return handlekurv.sort(function(a,b){return a.index - b.index;});
    }